package tech.mastertech.itau.drone.pedidos.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.drone.pedidos.dto.Drone;
import tech.mastertech.itau.drone.pedidos.dto.Endereco;
import tech.mastertech.itau.drone.pedidos.dto.Pedido;
import tech.mastertech.itau.drone.pedidos.dto.Perfil;
import tech.mastertech.itau.drone.pedidos.dto.Usuario;
import tech.mastertech.itau.drone.pedidos.repositories.DroneRepository;
import tech.mastertech.itau.drone.pedidos.repositories.EnderecoRepository;
import tech.mastertech.itau.drone.pedidos.repositories.PedidoRepository;
import tech.mastertech.itau.drone.pedidos.repositories.UsuarioRepository;
import tech.mastertech.itau.drone.pedidos.services.PedidoService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PedidoService.class, DroneService.class, EnderecoService.class} )
public class PedidoServiceTest {
	@Autowired
	private PedidoService sujeito;

	@MockBean
	private DroneRepository droneRepository;
	@MockBean
	private UsuarioRepository usuarioRepository;
	@MockBean
	private EnderecoRepository enderecoRepository;
	@MockBean
	private PedidoRepository pedidoRepository;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
	
	Pedido pedido;
	Drone drone;
	Usuario usuario;
	Endereco origem;
	Endereco destino;
	
	@Before
	public void preparar() {
		drone = new Drone();
		usuario = new Usuario();
		origem = new Endereco();
		destino = new Endereco();
		drone.setIdDrone(1);
		drone.setModelo("drone de teste");
		usuario.setIdUsuario(1);
		usuario.setNome("usuario de teste");
		origem.setIdEndereco(1);
		origem.setLogradouro("rua origem");
		origem.setCep("11111111");
		origem.setBairro("bairro origem");
		origem.setCidade("São Paulo");
		origem.setEstado("SP");
		origem.setPais("Brasil");
		origem.setNumero(1);
		origem.setLongitude("0");
		origem.setLatitude("0");
		origem.setComplemento("complemento origem");
		destino.setIdEndereco(2);
		destino.setLogradouro("rua destino");
		destino.setCep("22222222");
		destino.setBairro("bairro destino");
		destino.setCidade("São Paulo");
		destino.setEstado("SP");
		destino.setPais("Brasil");
		destino.setNumero(2);
		destino.setLongitude("1");
		destino.setLatitude("1");
		destino.setComplemento("complemento destino");
		pedido = new Pedido();
		pedido.setValor(15.0);
		pedido.setData(LocalDate.parse("2019-05-29", formatter));
		pedido.setDrone(drone);
		pedido.setUsuario(usuario);
		pedido.setOrigem(origem);
		pedido.setDestino(destino);

	}

	@Test
	public void deveCriarUmPedido() {

		Usuario usuarioLogado = new Usuario();
		usuarioLogado.setEmail("usuario@teste.com");
		usuarioLogado.setPerfil(Perfil.ADMIN);
		
		when(pedidoRepository.save(pedido)).thenReturn(pedido);
		when(droneRepository.findById(1)).thenReturn(Optional.of(pedido.getDrone()));
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(pedido.getUsuario()));
		when(usuarioRepository.findById(2)).thenReturn(Optional.of(usuarioLogado));
		when(enderecoRepository.save(pedido.getOrigem())).thenReturn(pedido.getOrigem());
		when(enderecoRepository.save(pedido.getDestino())).thenReturn(pedido.getDestino());
		
		Pedido pedidoCriado = sujeito.setPedido(pedido, "2");
		  
		assertEquals(pedido.getData(), pedidoCriado.getData());
		assertEquals(pedido.getDestino(), pedidoCriado.getDestino());
		assertEquals(pedido.getUsuario(), pedidoCriado.getUsuario());
		assertEquals(pedido.getValor(), pedidoCriado.getValor(), 0.1);
		assertEquals(pedido.getOrigem(), pedidoCriado.getOrigem());
	}
	
	@Test(expected = Exception.class)
	public void naoDeveCriarUmPedidoQuandoUsuarioNaoTiverPerfilAdmin() {

		Usuario usuarioLogado = new Usuario();
		usuarioLogado.setEmail("usuario@teste.com");
		usuarioLogado.setPerfil(Perfil.CLIENTE);
		
		when(usuarioRepository.findByEmail("usuario@teste.com")).thenReturn(Optional.of(usuarioLogado));

		sujeito.setPedido(pedido, "usuario@teste.com");
	}
	
	@Test
	public void deveRetornarUmaListaDePedidosDoUsuario() {
		when(usuarioRepository.findById(pedido.getUsuario().getIdUsuario())).thenReturn(Optional.ofNullable(pedido.getUsuario()));
		when(pedidoRepository.findAllByUsuario(pedido.getUsuario())).thenReturn(Lists.list(pedido));
	    
	    Iterable<Pedido> pedidosIterable = sujeito.getPedidosByUsuario(pedido.getUsuario().getIdUsuario());
	    List<Pedido> pedidosEncontrados = Lists.newArrayList(pedidosIterable);
	    
	    assertEquals(1, pedidosEncontrados.size());
	    assertEquals(pedido, pedidosEncontrados.get(0));
	}
	
	@Test
	public void deveRetornarUmaListaDePedidosDoDrone() {
		when(droneRepository.findById(drone.getIdDrone())).thenReturn(Optional.ofNullable(drone));
		when(pedidoRepository.findAllByDrone(drone)).thenReturn(Lists.list(pedido));
	    
	    Iterable<Pedido> pedidosIterable = sujeito.getPedidosByDrone(drone.getIdDrone());
	    List<Pedido> pedidosEncontrados = Lists.newArrayList(pedidosIterable);
	    
	    assertEquals(1, pedidosEncontrados.size());
	    assertEquals(pedido, pedidosEncontrados.get(0));
	}
	
	@Test
	public void deveRetornarUmaListaDePedidos() {
		when(pedidoRepository.findAll()).thenReturn(Lists.list(pedido));
	    
	    Iterable<Pedido> pedidosIterable = sujeito.getPedidos();
	    List<Pedido> pedidosEncontrados = Lists.newArrayList(pedidosIterable);
	    
	    assertEquals(1, pedidosEncontrados.size());
	    assertEquals(pedido, pedidosEncontrados.get(0));
	}

}
