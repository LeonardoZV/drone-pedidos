package tech.mastertech.itau.drone.pedidos;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.drone.pedidos.DronePedidosApplication;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DronePedidosApplicationTests {

	@Test
	public void contextLoads() {
		DronePedidosApplication.main(new String[0]);
	}

}
