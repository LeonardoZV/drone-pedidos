package tech.mastertech.itau.drone.pedidos.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.time.format.DateTimeFormatter;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.drone.pedidos.controllers.PedidoController;
import tech.mastertech.itau.drone.pedidos.dto.Drone;
import tech.mastertech.itau.drone.pedidos.dto.Endereco;
import tech.mastertech.itau.drone.pedidos.dto.Pedido;
import tech.mastertech.itau.drone.pedidos.dto.Usuario;
import tech.mastertech.itau.drone.pedidos.services.PedidoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PedidoController.class)
public class PedidoControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private PedidoService pedidoService;
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
	
	private ObjectMapper mapper = new ObjectMapper();
	
	Pedido pedido;
	Drone drone;
	Usuario usuario;
	Endereco origem;
	Endereco destino;
	
	@Before
	public void preparar() {
		drone = new Drone();
		usuario = new Usuario();
		origem = new Endereco();
		destino = new Endereco();
		drone.setIdDrone(1);
		drone.setModelo("drone de teste");
		usuario.setIdUsuario(1);
		usuario.setNome("usuario de teste");
		origem.setIdEndereco(1);
		origem.setLogradouro("rua origem");
		origem.setCep("11111111");
		origem.setBairro("bairro origem");
		origem.setCidade("São Paulo");
		origem.setEstado("SP");
		origem.setPais("Brasil");
		origem.setNumero(1);
		origem.setLongitude("0");
		origem.setLatitude("0");
		origem.setComplemento("complemento origem");
		destino.setIdEndereco(2);
		destino.setLogradouro("rua destino");
		destino.setCep("22222222");
		destino.setBairro("bairro destino");
		destino.setCidade("São Paulo");
		destino.setEstado("SP");
		destino.setPais("Brasil");
		destino.setNumero(2);
		destino.setLongitude("1");
		destino.setLatitude("1");
		destino.setComplemento("complemento destino");
		pedido = new Pedido();
		pedido.setValor(15.0);
		pedido.setDrone(drone);
		pedido.setUsuario(usuario);
		pedido.setOrigem(origem);
		pedido.setDestino(destino);

	}

	@Test
	@WithMockUser(username= "1")
	public void deveCriarUmPedido() throws Exception {
		when(pedidoService.setPedido(any(Pedido.class), any(String.class))).thenReturn(pedido);
		    
		String pedidoJson = mapper.writeValueAsString(pedido);
		    
		mockMvc.perform(post("/dronedelivery/pedido")
		        .contentType(MediaType.APPLICATION_JSON_UTF8)
		        .content(pedidoJson))
		        .andExpect(status().isCreated())
		        .andExpect(content().string(pedidoJson));
	}
	
	@Test
	@WithMockUser(username= "1")
	public void deveRetornarUmaListaDePedidos() throws Exception {
		List<Pedido> pedidos = Lists.list(pedido);
		
		when(pedidoService.getPedidos()).thenReturn(pedidos);		    
		    
		mockMvc.perform(get("/dronedelivery/pedidos"))
        .andExpect(status().isOk())
        .andExpect(content().string(mapper.writeValueAsString(pedidos)));
	}
	
	@Test
	@WithMockUser(username= "1")
	public void deveRetornarUmaListaDePedidosDeUmUsuario() throws Exception {
		int idUsuario = 1;
		
		List<Pedido> pedidos = Lists.list(pedido);
		
		when(pedidoService.getPedidosByUsuario(idUsuario)).thenReturn(pedidos);		    
		    
		mockMvc.perform(get("/dronedelivery/pedidos?idUsuario=" + idUsuario))
        .andExpect(status().isOk())
        .andExpect(content().string(mapper.writeValueAsString(pedidos)));
	}
	
	@Test
	@WithMockUser(username= "1")
	public void deveRetornarUmaListaDePedidosDeUmDrone() throws Exception {
		int idDrone = 1;
		
		List<Pedido> pedidos = Lists.list(pedido);
		
		when(pedidoService.getPedidosByDrone(idDrone)).thenReturn(pedidos);		    
		    
		mockMvc.perform(get("/dronedelivery/pedidos?idDrone=" + idDrone))
        .andExpect(status().isOk())
        .andExpect(content().string(mapper.writeValueAsString(pedidos)));
	}

}
