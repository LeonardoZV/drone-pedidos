package tech.mastertech.itau.drone.pedidos.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.drone.pedidos.dto.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Integer> {
	Iterable<Pedido> findAllByIdUsuario(int idUsuario);
	Iterable<Pedido> findAllByIdDrone(int idDrone);
}