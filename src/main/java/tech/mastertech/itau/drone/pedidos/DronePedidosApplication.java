package tech.mastertech.itau.drone.pedidos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DronePedidosApplication {

	public static void main(String[] args) {
		SpringApplication.run(DronePedidosApplication.class, args);
	}

}
