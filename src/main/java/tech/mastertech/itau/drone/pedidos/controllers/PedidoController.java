package tech.mastertech.itau.drone.pedidos.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.drone.pedidos.dto.Pedido;
import tech.mastertech.itau.drone.pedidos.services.PedidoService;

@RestController
@CrossOrigin
public class PedidoController {
	@Autowired
	private PedidoService pedidoService;
	
	@GetMapping("/pedidos")
	public Iterable<Pedido> getPedidos(@RequestParam(required = false) Integer idUsuario, @RequestParam(required = false) Integer idDrone){
		if(idUsuario != null) {
			return pedidoService.getPedidosByUsuario(idUsuario);
		}		
		if(idDrone != null) {
			return pedidoService.getPedidosByDrone(idDrone);
		}
		return pedidoService.getPedidos();
	}
	
	@PostMapping("/pedido")
	@ResponseStatus(HttpStatus.CREATED)
	public Pedido setPedido(@RequestBody Pedido pedido, Principal principal) {
		return pedidoService.setPedido(pedido, principal.getName());
	}
}
