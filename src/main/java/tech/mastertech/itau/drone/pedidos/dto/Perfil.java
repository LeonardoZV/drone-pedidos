package tech.mastertech.itau.drone.pedidos.dto;

public enum Perfil {
	ADMIN,
	CLIENTE;
}
