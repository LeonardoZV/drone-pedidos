package tech.mastertech.itau.drone.pedidos.services;

import java.time.LocalDate;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.drone.pedidos.dto.Drone;
import tech.mastertech.itau.drone.pedidos.dto.Endereco;
import tech.mastertech.itau.drone.pedidos.dto.Pedido;
import tech.mastertech.itau.drone.pedidos.dto.Perfil;
import tech.mastertech.itau.drone.pedidos.dto.Usuario;
import tech.mastertech.itau.drone.pedidos.repositories.DroneRepository;
import tech.mastertech.itau.drone.pedidos.repositories.PedidoRepository;
import tech.mastertech.itau.drone.pedidos.repositories.UsuarioRepository;

@Service
public class PedidoService {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	@Autowired
	private EnderecoService enderecoService;
	@Autowired
	private DroneRepository droneRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;

	public Iterable<Pedido> getPedidos(){
		Iterable<Pedido> pedidos = pedidoRepository.findAll();
		return pedidos;
	}
	
	public Iterable<Pedido> getPedidosByUsuario(int idUsuario){
		Usuario usuario = usuarioRepository.findById(idUsuario).get();
		Iterable<Pedido> pedidos = pedidoRepository.findAllByUsuario(usuario);		
		return pedidos;
	}
	
	public Iterable<Pedido> getPedidosByDrone(int idDrone){
		Drone drone = droneRepository.findById(idDrone).get();
		Iterable<Pedido> pedidos = pedidoRepository.findAllByDrone(drone);		
		return pedidos;
	}
	
	public Pedido setPedido(Pedido pedido, String idUsuario) {
		Perfil perfil = usuarioRepository.findById(Integer.valueOf(idUsuario)).get().getPerfil();
		if (perfil.name() != Perfil.ADMIN.name()) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
		
		Drone drone = droneRepository.findById(pedido.getDrone().getIdDrone()).get();
		Usuario usuario = usuarioRepository.findById(pedido.getUsuario().getIdUsuario()).get();
		Endereco origem = enderecoService.cadastraEndereco(pedido.getOrigem());
		Endereco destino = enderecoService.cadastraEndereco(pedido.getDestino());

		pedido.setCodigoPedido(gerarCodigoPedido());
		pedido.setDrone(drone);
		pedido.setUsuario(usuario);
		pedido.setOrigem(origem);
		pedido.setDestino(destino);
		pedido.setData(LocalDate.now());
		
		pedidoRepository.save(pedido);
		return pedido;
	}
	
	private String gerarCodigoPedido() {
		UUID uuid = UUID.randomUUID();
	    return uuid.toString();
	}
}
