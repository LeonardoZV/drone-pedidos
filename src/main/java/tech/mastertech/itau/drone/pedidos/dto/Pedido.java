package tech.mastertech.itau.drone.pedidos.dto;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Pedido {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPedido;
	@NotBlank
    private String codigoPedido;
	@NotNull
    private int idDrone;
	@NotNull
    private int idUsuario;
	@NotNull
    private double valor;
	@NotNull
    private int idEnderecoOrigem;
	@NotNull
    private int idEnderecoDestino;
	@NotNull
	private LocalDate data;
	
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public String getCodigoPedido() {
		return codigoPedido;
	}
	public void setCodigoPedido(String codigoPedido) {
		this.codigoPedido = codigoPedido;
	}
	public int getIdDrone() {
		return idDrone;
	}
	public void setIdDrone(int idDrone) {
		this.idDrone = idDrone;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public int getIdEnderecoOrigem() {
		return idEnderecoOrigem;
	}
	public void setIdEnderecoOrigem(int idEnderecoOrigem) {
		this.idEnderecoOrigem = idEnderecoOrigem;
	}
	public int getIdEnderecoDestino() {
		return idEnderecoDestino;
	}
	public void setIdEnderecoDestino(int idEnderecoDestino) {
		this.idEnderecoDestino = idEnderecoDestino;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}	
}
